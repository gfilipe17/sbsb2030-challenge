const axios = require('axios');
const cheerio = require('cheerio');
const fs = require('fs');
const chalk = require('chalk');

const baseUrl = 'http://licitacoes.ssp.df.gov.br';
const url = `${baseUrl}/index.php/licitacoes`;
const outputFile = 'data.json';
const parsedResults = [];

// Contador de total de arquivos do site
var totalFiles = 0;

console.log(chalk.yellow.bgBlue(`\n  Scraping of ${chalk.underline.bold(url)} initiated...\n`))

// Remove do título 'Enter' e 'Tabs' que existiam no HTML
const regexName = (name) => name.replace(/\n/g, '').replace(/\t/g, '');

// Busca o conteúdo da página principal
const getWebsiteContent = async (url) => {
    try {
        let response = await axios.get(url);
        let $ = cheerio.load(response.data);
        let metadata = await getFoldersByPage($);
        parsedResults.push(metadata);
        exportResults(parsedResults);
    } catch (error) {
        exportResults(parsedResults);
        console.error(error);
    }
}

// Busca conteúdo de subníveis de páginas formando a estrutura json de retorno
const getFoldersByPage = async ($) => {
    // Todos links [pastas] disponíveis
    let listLinks = $('#dm_cats .dm_title a');
    let folders = [];

    // Iteração de cada subnível
    for (let i = 0; i < listLinks.length; i++) {
        let url = listLinks[i].attribs.href;
        let name = regexName(listLinks[i].children[0].data);

        // Para cada subnível buscar o conteúdo e preparar sua estrutura json
        let attachments = await getChildrenWebsiteContent(url);

        let metadata = {
            name,
            totalFiles,
            url: `${baseUrl}${url}`,
            attachments
        }

        // Retorno da estrutura de subnível
        folders.push(metadata)
    }

    return folders;
}

// Busca o conteudo de um subnível recursivamente, montando sua estrutura json
const getChildrenWebsiteContent = async (url) => {
    let files = [];
    let counter = 0;
    let actualPage = 0;
    let metadata = [];
    let requestUrl = `${baseUrl}${url}`;

    // Inicialmente ambos com 0 [actualPage & counter], pois sempre entrará ao menos uma vez
    for (actualPage; actualPage <= counter; actualPage++) {
        let response = await axios.get(requestUrl);
        let $ = cheerio.load(response.data);

        // Pega o contador de acordo com a paginação da página
        counter = $('.pagination ul li').length ?
            $('.pagination ul li').length - 4 :
            0;

        console.log(chalk.cyan(`  Scraping: ${actualPage || '1'} of ${counter || '1'} - ${requestUrl}`))

        // Pega a página atual de acordo a paginação da página
        $('span.pagenav').each((i, item) => {
            let actual = $(item).text();
            if (Number(actual)) actualPage = actual;
        });

        // Pega a próxima página de acordo com a paginação da página
        let nextPageLink = `${baseUrl}${$('.pagination-next a').attr('href')}`;
        
        // Estrutura json
        $('#dm_docs .dm_row.dm_light').map((i, el) => {
            let name = regexName($(el).find('a').eq(0).text());
            let url = $(el).find('.dm_title a').attr('href');
            let description = $(el).find('.dm_description span').text() || $(el).find('.dm_description p').text();
            let published = $(el).find('.dm_details table td').eq(1).text();
            let file = {
                name,
                description,
                published,
                url: `${baseUrl}${url}`
            };
            totalFiles++;
            files.push(file);
        });

        // Verifica se na página atual possuem mais subníveis
        metadata = await getFoldersByPage($);
        
        // Se ainda tiver páginas para percorrer, utilizará o link da próxima página na próxima iteração
        if (actualPage < counter) requestUrl = nextPageLink;
    }

    return [
        ...files,
        ...metadata
    ];
}

// Gera um arquivo .json a partir do resultado da raspagem de dados
const exportResults = (parsedResults) => {
    fs.writeFile(outputFile, JSON.stringify(parsedResults, null, 4), (err) => {
        if (err) console.log(err)
        console.log(chalk.yellow.bgBlue(`\n ${chalk.underline.bold(parsedResults.length)} Results exported successfully to ${chalk.underline.bold(outputFile)}\n`))
    })
}

// Chamada início do código
getWebsiteContent(url)